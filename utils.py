import math
# from spiral import Spiral


def calculate_length(first_point, second_point):
    first_x = first_point[0]
    first_y = first_point[1]
    second_x = second_point[0]
    second_y = second_point[1]

    return math.sqrt(pow(first_x-second_x, 2)+pow(first_y-second_y, 2))

# second return value means whteher or not it's perpendicular
#
#######


def calculate_slope(first_point, second_point):
    # (y2 - y1)/(x2 - x1)
    # second return value means whteher or not it's perpendicular
    #
    #######
    if (second_point[0]-first_point[0]) != 0:
        return (second_point[1]-first_point[1])/(second_point[0]-first_point[0]), False
    else:
        return 0, True


def calculate_angle(first_point, second_point, third_point):
    m_1 = calculate_slope(first_point, second_point)
    m_2 = calculate_slope(second_point, third_point)

    return math.atan(abs((m_2-m_1)/(1+(m_2*m_1))))


# Python3 code to find all three angles
# of a triangle given coordinate
# of all three vertices

# returns square of distance b/w two points

def lengthSquare(X, Y):
    xDiff = X[0] - Y[0]
    yDiff = X[1] - Y[1]
    return xDiff * xDiff + yDiff * yDiff


def get_middle_angle(A, B, C):

    # Square of lengths be a2, b2, c2
    a2 = lengthSquare(B, C)
    b2 = lengthSquare(A, C)
    c2 = lengthSquare(A, B)

    # length of sides be a, b, c
    a = math.sqrt(a2)
    c = math.sqrt(c2)

    if a == 0 and c == 0:
        return 0

    # From Cosine law
    betta = math.acos((a2 + c2 - b2) /
                      (2 * a * c))

    # Converting to degree
    # alpha = alpha * 180 / math.pi
    # betta = betta * 180 / math.pi
    # gamma = gamma * 180 / math.pi

    return betta


# def calculate_next_point(first_point, second_point, betta, ratio, point_num):
#     l_2_len = calculate_length(first_point=first_point,
#                                second_point=second_point)
#     l_new_len = l_2_len*ratio

#     c = math.sqrt(pow(l_2_len, 2)+pow(l_new_len, 2) -
#                   (2*l_2_len*l_new_len*math.cos(betta)))

#     fi = math.acos((second_point[0]-first_point[0])/l_2_len)

#     ti = math.asin(l_new_len*math.sin(betta)/c)

#     ni = fi+ti

#     x_delta = math.cos(ni) * c
#     y_delta = math.sin(ni) * c

#     x_new = first_point[0] + x_delta
#     y_new = first_point[1] + y_delta

#     return (int(x_new), int(y_new)), l_new_len

# def calculate_next_point(first_point, second_point, betta, ratio, point_num):

#     if first_point[0] == second_point[0] and first_point[1] == second_point[1]:
#         return first_point, 0

#     l_2_len = calculate_length(first_point=first_point,
#                                second_point=second_point)
#     l_new_len = l_2_len*ratio

#     c = math.sqrt(pow(l_2_len, 2)+pow(l_new_len, 2) -
#                   (2*l_2_len*l_new_len*math.cos(betta)))

#     fi = math.acos((second_point[0]-first_point[0])/l_2_len)

#     ti = math.asin(l_new_len*math.sin(betta)/c)

#     ni = fi+ti

#     angle = 2*math.pi + ni - betta

#     x_delta = math.cos(angle) * l_new_len
#     y_delta = math.sin(angle) * l_new_len

#     x_new = second_point[0] + x_delta
#     y_new = second_point[1] + y_delta

#     new_point = (int(x_new), int(y_new))

#     test_result_angle = get_middle_angle(first_point, second_point, new_point)

#     print('test_result_angle: ', math.degrees(test_result_angle),
#           'original betta: ', math.degrees(betta))

#     return new_point, l_new_len

def get_line_through_points(first_point, second_point):
    # (𝑦𝐴−𝑦𝐵)𝑥−(𝑥𝐴−𝑥𝐵)𝑦+𝑥𝐴𝑦𝐵−𝑥𝐵𝑦𝐴=0.
    # this function return a, b, c as used for line definition ax+by+c=0
    #
    return first_point[1]-second_point[1], second_point[0]-first_point[0], (first_point[0]*second_point[1])-(second_point[0]*first_point[1])


def get_line_perpendicular_to_points_through_point(first_point, second_point, through_point):
    a, b, c = get_line_through_points(
        first_point=first_point, second_point=second_point)

    # TODO use the point through which it goes
    x = through_point[0]
    y = through_point[1]

    #  bx - ay+ k = 0, where k = ac, is an arbitrary constant.
    return b, -a, (a*y)-(b*x)


def get_line_perpendicular_to_points(first_point, second_point):

    x = (first_point[0]+second_point[0])/2
    y = (first_point[1]+second_point[1])/2
    through_point = (x, y)

    #  bx - ay+ k = 0, where k = ac, is an arbitrary constant.
    return get_line_perpendicular_to_points_through_point(first_point, second_point, through_point)


def get_lines_intersection(a_1, b_1, c_1, a_2, b_2, c_2):

    x = ((b_1*c_2)-(b_2*c_1))/((a_1*b_2)-(a_2*b_1))
    y = ((c_1*a_2)-(c_2*a_1))/((a_1*b_2)-(a_2*b_1))

    return (x, y)


def flip_point_over(point, anchor):
    x_delta = anchor[0] - point[0]
    y_delta = anchor[1] - point[1]
    x = anchor[0] + x_delta
    y = anchor[1] + y_delta
    return (x, y)


def are_lines_parallel(a_1, b_1, a_2, b_2):
    if a_2 == 0 or b_2 == 0:
        return False
    # a₁/a₂ = b₁/b₂
    return a_1/a_2 == b_1/b_2


# def calculate_next_point(first_point, second_point, third_point, angle):
#     # second return value means whether or not angle is 90 degress and nothing can be drawn
#     #
#     ######
#     a_1, b_1, c_1 = get_line_through_points(
#         first_point=first_point, second_point=second_point)
#     a_2, b_2, c_2 = get_line_perpendicular_to_points(second_point, third_point)

#     if are_lines_parallel(a_1, b_1, a_2, b_2):
#         return (0, 0), 0, True

#     intersection = get_lines_intersection(a_1, b_1, c_1, a_2, b_2, c_2)
#     if angle < math.pi/2:
#         return intersection, calculate_length(first_point=third_point,
#                                               second_point=intersection), False
#     else:
#         flipped_point = flip_point_over(intersection, third_point)
#         return flipped_point, calculate_length(first_point=third_point,
#                                                second_point=flipped_point), False

def resize_intersection(second_point, third_point, intersection, ratio):
    last_line_len = calculate_length(first_point=second_point,
                                     second_point=third_point)
    desired_len = last_line_len * ratio
    actual_len = calculate_length(first_point=third_point,
                                  second_point=intersection)
    len_ratio = desired_len/actual_len
    x_delta = intersection[0]-third_point[0]
    y_delta = intersection[1]-third_point[1]
    x = x_delta * len_ratio
    y = y_delta * len_ratio
    return (third_point[0]+x, third_point[1]+y)


def calculate_next_point(first_point, second_point, third_point, angle, ratio):
    # second return value means whether or not angle is 90 degress and nothing can be drawn
    #
    ######
    a_1, b_1, c_1 = get_line_through_points(
        first_point=first_point, second_point=second_point)
    a_2, b_2, c_2 = get_line_perpendicular_to_points(second_point, third_point)

    if are_lines_parallel(a_1, b_1, a_2, b_2):
        return (0, 0), 0, True

    intersection = get_lines_intersection(a_1, b_1, c_1, a_2, b_2, c_2)
    if angle < math.pi/2:
        inter_res = resize_intersection(second_point=second_point,
                                        third_point=third_point, intersection=intersection, ratio=ratio)
        return inter_res, calculate_length(first_point=third_point,
                                           second_point=inter_res), False
    else:
        flipped_point = flip_point_over(intersection, third_point)
        inter_res = resize_intersection(second_point=second_point,
                                        third_point=third_point, intersection=flipped_point, ratio=ratio)
        return inter_res, calculate_length(first_point=third_point,
                                           second_point=inter_res), False


def get_color(value):
    #   Input parameter should be between 0 and 1
    #   Returns R G B
    #
    ######################
    if value < 1/6:
        red = 255
        green = value * 6*127
        blue = 0
        return red, green, blue
    elif 1/6 <= value and value < 2/6:
        red = 255
        green = 127 + ((value-(1/6))*6*128)
        blue = 0
        return red, green, blue
    elif 2/6 <= value and value < 3/6:
        red = 255 - ((value-(2/6))*6*255)
        green = 255
        blue = 0
        return red, green, blue
    elif 3/6 <= value and value < 4/6:
        red = 0
        green = 255 - ((value-(3/6))*6*255)
        blue = ((value-(3/6))*6*255)
        return red, green, blue
    elif 4/6 <= value and value < 5/6:
        red = ((value-(4/6))*6*75)
        green = 0
        blue = 255-((value-(4/6))*6*125)
        return red, green, blue
    elif 5/6 <= value and value <= 1:
        red = 75+((value-(5/6))*6*73)
        green = 0
        blue = 130+((value-(5/6))*6*81)
        return red, green, blue


def calculate_frequency(basic_freq, value):
    #
    # example basic_freq = 440 hz
    #  value should be between 0 and 1
    ###########
    return basic_freq*math.pow(2, value)


def culaculate_sub_spiral(first_point, second_point, third_point):
    a_1, b_1, c_1 = get_line_through_points(
        first_point=first_point, second_point=third_point)
    a_2, b_2, c_2 = get_line_perpendicular_to_points_through_point(
        first_point, third_point, second_point)
    intersection = get_lines_intersection(a_1, b_1, c_1, a_2, b_2, c_2)
    flipped_point = flip_point_over(intersection, second_point)
    return [third_point, second_point, flipped_point]
