import utils
import cv2
import math


class Spiral:

    def __init__(self, points, start_thickness, oscilator=None):

        self.first_point = points[0]
        self.second_point = points[1]
        self.third_point = points[2]
        self.start_thickness = start_thickness
        self.oscilator = oscilator

        self.lines = [(self.first_point, self.second_point),
                      (self.second_point, self.third_point)]

        betta = utils.get_middle_angle(
            self.first_point, self.second_point, self.third_point)
        self.angle = betta

        first_line_len = utils.calculate_length(
            self.first_point, self.second_point)
        second_line_len = utils.calculate_length(
            self.second_point, self.third_point)
        self.ratio = min(first_line_len, second_line_len) / \
            max(first_line_len, second_line_len)

        if oscilator is not None:
            freq = utils.calculate_frequency(440, self.angle/math.pi)
            oscilator.setFreq(freq)

    def draw(self, image):

        # print('angle: ', math.degrees(self.angle))

        image_hight, image_width, _ = image.shape

        self.__calculate(image_hight, image_width)
        num_lines = len(self.lines)

        for index, line in enumerate(self.lines):
            # print('index: ', index)
            line_wid = int((num_lines-index)/num_lines *
                           self.start_thickness)+1

            red, green, blue = utils.get_color(self.angle/math.pi)

            cv2.line(image, (int(line[0][0]), int(line[0][1])), (int(line[1][0]), int(line[1][1])),
                     (blue, green, red), thickness=line_wid)

        # self.sub_spirals = []

        # for index, line in enumerate(self.lines):
        #     if index == 0:
        #         continue
        #     if index == num_lines-1:
        #         break
        #     points = utils.culaculate_sub_spiral(
        #         self.lines[index-1][0], line[0], line[1])
        #     line_wid = int((num_lines-index)/num_lines * 5)+1
        #     sub_spiral = Spiral(points, line_wid)

        #     self.sub_spirals.append(sub_spiral)

        # for sub_spiral in self.sub_spirals:
        #     sub_spiral.draw(image)

    # Declaring private method
    def __calculate(self, image_hight, image_width):

        dist = utils.calculate_length(self.lines[-1][0], self.lines[-1][1])
        counter = 0
        while True:

            # if counter > 50:
            #     break
            last_line = self.lines[-1]
            two_back_line = self.lines[-2]

            new_point, dist, perpendicular = utils.calculate_next_point(two_back_line[0],
                                                                        last_line[0], last_line[1], self.angle, self.ratio)
            # print('dist: ', dist)
            if dist > 5 * max(image_width, image_hight):
                break
            if perpendicular == True:
                break
            if dist < 5:
                break

            self.lines.append((last_line[1], new_point))
            counter += 1
