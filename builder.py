from spiral import Spiral
# import mediapipe as mp
# mp_drawing = mp.solutions.drawing_utils
# mp_drawing_styles = mp.solutions.drawing_styles
# mp_pose = mp.solutions.pose


def construct_spiral(landmarks, results, image_width, image_hight, oscilator=None):
    x_first = results.pose_landmarks.landmark[landmarks[0]].x * image_width
    y_first = results.pose_landmarks.landmark[landmarks[0]].y * image_hight

    x_second = results.pose_landmarks.landmark[landmarks[1]].x * image_width
    y_second = results.pose_landmarks.landmark[landmarks[1]].y * image_hight

    x_third = results.pose_landmarks.landmark[landmarks[2]].x * image_width
    y_third = results.pose_landmarks.landmark[landmarks[2]].y * image_hight

    third_point = (x_third, y_third)
    second_point = (x_second, y_second)
    first_point = (x_first, y_first)

    return Spiral(
        [first_point, second_point, third_point], start_thickness=5, oscilator=oscilator)
