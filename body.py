import builder
from spiral import Spiral
import cv2
import mediapipe as mp
from pyo import *
import pathlib
import subprocess

save_video = False
if int(os.getenv('CREATE_VIDEO')) == 1:
    save_video = True

s = Server(audio="coreaudio").boot()

if save_video == True:
    # Path of the recorded sound file.
    current_dir = pathlib.Path(__file__).parent
    path = os.path.join(current_dir, "synth.wav")
    # Record for 10 seconds a 24-bit wav file.
    s.recordOptions(dur=-1, filename=path, fileformat=0,
                    sampletype=0, quality=1.0)

osc1 = Sine(freq=440).out()
osc2 = Sine(freq=810).out()

if save_video == True:
    s.recstart()

s.start()

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose


# For webcam input:
cap = cv2.VideoCapture(0)

# We need to check if camera
# is opened previously or not
if (cap.isOpened() == False):
    print("Error reading video file")

# We need to set resolutions.
# so, convert them from float to integer.
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))

size = (frame_width, frame_height)

# Below VideoWriter object will create
# a frame of above defined The output
# is stored in 'filename.avi' file.
result = None
FPS = 30
if save_video == True:
    result = cv2.VideoWriter('muted_video.avi',
                             cv2.VideoWriter_fourcc(*'MJPG'),
                             FPS, size)

start_time = time.time()
num_im_image = 0


with mp_pose.Pose(
        min_detection_confidence=0.8,
    model_complexity=2,
    enable_segmentation=True,
        min_tracking_confidence=0.8) as pose:
    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = pose.process(image)

        image_hight, image_width, _ = image.shape

        left_arm_spiral = None
        right_arm_spiral = None
        if results.pose_landmarks is not None:

            left_arm_spiral = builder.construct_spiral(
                (mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST), results, image_width, image_hight, osc1)
            right_arm_spiral = builder.construct_spiral(
                (mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST), results, image_width, image_hight, osc2)

        # Draw the pose annotation on the image.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        # mp_drawing.draw_landmarks(
        #     image,
        #     results.pose_landmarks,
        #     mp_pose.POSE_CONNECTIONS,
        #     landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())

        if results.pose_landmarks is not None:
            left_arm_spiral.draw(image)
            right_arm_spiral.draw(image)

        # Flip the image horizontally for a selfie-view display.
        flipped_im = cv2.flip(image, 1)

        # Write the frame into the
        # file 'filename.avi'
        if save_video == True:
            per_image_dur = 1/float(FPS)
            elapsed_time = time.time() - start_time
            expected_im_num = int(elapsed_time/per_image_dur)
            new_im_count = expected_im_num - num_im_image

            i = 0
            while i < new_im_count:
                result.write(flipped_im)
                num_im_image += 1
                i += 1

        cv2.imshow('MediaPipe Pose', flipped_im)
        if cv2.waitKey(5) & 0xFF == 27:
            break
cap.release()
if save_video == True:
    result.release()
    s.recstop()
    cmd = 'ffmpeg -y -i synth.wav  -r '+str(FPS) + \
        ' -i muted_video.avi  -filter:a aresample=async=1 -c:a flac -c:v copy full_video.mkv'
    subprocess.call(cmd, shell=True)

# Closes all the frames
cv2.destroyAllWindows()
